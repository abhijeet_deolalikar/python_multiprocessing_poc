from multiprocessing import Process, Pipe
import traceback
from example import func_a
from multiprocessing.dummy import Pool


class MyProcess(Process):
    def __init__(self, *args, **kwargs):
        Process.__init__(self, *args, **kwargs)
        self._pconn, self._cconn = Pipe()
        self._exception = None

    def run(self):
        try:
            print("Inside worker process")
            func_a(0)
            self._cconn.send(None)
        except Exception as e:
            tb = traceback.format_exc()
            self._cconn.send((e, tb))
            # raise e
            return

    def start(self):
        super().start()

    @property
    def process_status(self):
        if self._pconn.poll():
            self._exception = self._pconn.recv()
        return self._exception


def main():
    pool = Pool()
    p = MyProcess()
    print("before", p, p.is_alive())
    p.start()
    print("during", p, p.is_alive())

    def callback(_):
        print("Inside Parent Process Callback Method")
        if p.process_status:
            error, traceback = p.process_status
            print("Inside Parent Process: Stack trace of child process", traceback)
        else:
            print("Inside Parent Process: Successfully executed child process")

    result = pool.apply_async(_wait_for_process, (p,), {}, callback, callback)
    pool.close()
    pool.join()
    result.get(timeout=1)
    print("join", p, p.is_alive())
    print("complete main process")
    return
def _wait_for_process(process):
    print("Inside wait for process join method to complete")
    process.join()


if __name__ == "__main__":
    main()
