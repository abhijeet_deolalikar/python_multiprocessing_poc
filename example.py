import traceback


def func_a(i):
    func_b(i)


def func_b(i):
    func_c(i)


def func_c(i):
    func_d(i)


def func_d(i):
    # try:
    #     print(5 / i)
    # except Exception as e:
    #     traceback.print_exc()
    #     raise e
    raise Exception("Exception Occured in the script")