from multiprocessing import Process, Queue
import traceback
from example import func_a
from multiprocessing.dummy import Pool


class MyProcess(Process):
    def __init__(self, *args, **kwargs):
        Process.__init__(self, *args, **kwargs)
        self.queue = Queue()
        self._exception = None

    def run(self):
        try:
            print("Inside worker process")
            func_a(0)
        except Exception as e:
            tb = traceback.format_exc()
            self.queue.put((e, tb))
            # raise e
        finally:
            self.queue.close()

    def start(self):
        super().start()

    @property
    def process_status(self):
        if not self.queue.empty():
            self._exception = self.queue.get()
        return self._exception


def main():
    pool = Pool()
    p = MyProcess()
    print("before", p, p.is_alive())
    p.start()
    print("during", p, p.is_alive())

    def callback(_):
        print("Inside Parent Process Callback Method")
        if p.process_status:
            error, traceback = p.process_status
            print("Inside Parent Process: Stack trace of child process", traceback)
        else:
            print("Inside Parent Process: Successfully executed child process")

    result = pool.apply_async(_wait_for_process, (p,), {}, callback, callback)
    pool.close()
    pool.join()
    result.get(timeout=1)
    print("join", p, p.is_alive())
    print("complete main process")


def _wait_for_process(process):
    print("Inside wait for process join method to complete")
    process.join()


if __name__ == "__main__":
    main()
